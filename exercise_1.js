/* 
(*) SALARY CALCULATOR

Constant
- oneDaySalary = 100.000

Input
- workingDay = 20

Todo
- salary = oneDaySalary * workingDay

Output
- salary = 2.000.000
*/

// (*) JAVASCRIPT CODE

const oneDaySalary = 100000;
let workingDay = 20;

let salary = oneDaySalary * workingDay;
console.log("🚀 ~ salary", salary);
