/* 
(*) DETERMINE THE RECTANGLE'S AREA AND PERIMETER
Input
- length = 12
- width = 5

Todo
- area = length * width
- perimeter = (length + width) * 2

Output
- area = 60
- perimeter = 34 

*/

// (*) JAVASCRIPT CODE

let length = 12;
let width = 5;

let area = length * width;
let perimeter = (length + width) * 2;

console.log("🚀 ~ area", area);
console.log("🚀 ~ perimeter", perimeter);
