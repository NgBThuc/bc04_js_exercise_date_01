/* 
(*) CURRENCY EXCHANGE

Constant
- usdPrice = 23.500

Input
- usdAmount = 100

Todo
- vndAmount = usdPrice * usdAmout

Output
- vndAmount = 2.350.000

*/

// (*) JAVASCRIPT CODE
const usdPrice = 23500;
let usdAmount = 100;

let vndAmount = usdAmount * usdPrice;

console.log("🚀 ~ vndAmount", vndAmount);
