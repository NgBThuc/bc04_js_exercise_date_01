/* 
(*) CALCULATE THE AVERAGE
Input
- num1 = 1
- num2 = 2
- num3 = 3
- num4 = 4
- num5 = 5

Todo
- avarage = (num1 + num2 + num3 + num4 + num5) / 5

Output
- average = 3

*/

// (*) JAVASCRIPT CODE
let [num1, num2, num3, num4, num5] = [1, 2, 3, 4, 5];

let average = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("🚀 ~ average", average);
