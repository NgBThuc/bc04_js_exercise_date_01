/* 
(*) SUM 2 DIGITS
Input
- 36

Todo
- ten = Math.floor(36 / 10)
- unit = 36 % 10
- sum = ten + unit

Output
- sum = 9

*/

// (*) JAVASCRIPT CODE

let number = 36;
let ten = Math.floor(number / 10);
let unit = number % 10;

let sum = ten + unit;
console.log("🚀 ~ sum", sum);
